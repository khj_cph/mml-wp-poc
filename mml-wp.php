<?php
/*
Plugin Name: MML-WP
Plugin URI: https://www.molamil.com/
Description: Create some websites.
Version: 0.0.1
Author: KHJ
Author URI: https://www.molamil.com/
Copyright: Molamil
Text Domain: mml
Domain Path: /lang
*/

//require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Container\Container;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Molamil\Controllers\MML_REST_Pages_Controller;
use Molamil\Controllers\MML_REST_Site_Controller;

if( ! defined( 'ABSPATH' ) ) exit;
//if( ! class_exists('MML') ) :

        function main( ) 
        {
                $app = new Container( );

                // bindings should be defineable by devs in some sort of service registration...
                $app->bind(
                        'Molamil\Interfaces\IPagesRepository',
                        'Molamil\Repositories\PagesRepository'
                );

                $pages_controller = $app->make('Molamil\Controllers\MML_REST_Pages_Controller');
                $site_controller  = $app->make('Molamil\Controllers\MML_REST_Site_Controller');

                // refactor all this stuff!!! -----------------
                // of course, in some loop init...
                $controllers = [$pages_controller, $site_controller];
                add_action('rest_api_init', [$controllers[0], 'register_routes'], 5);
                add_action('rest_api_init', [$controllers[1], 'register_routes'], 5);

                // somewhere not here...
                add_action( 'rest_api_init', function( ) {
                        add_filter( 'acf/format_value/type=post_object', 
                        function( $value, $post_id, $field ) 
                        {
                                $type_object = get_post_type_object( $field['post_type'][0] );
                                $base = $type_object->rest_base;
                                $path = wp_make_link_relative( get_permalink( $value ) );

                                return [
                                        'type' => $type_object->name,
                                        'path' => $base . rtrim( $path, '/' )
                                ];
                        }, 10, 4 );
                }, 5 );
                // ----------------------

                add_action( 'init', function( ) {
                        if( function_exists( 'acf_add_options_page' ) )
                        {
                                acf_add_options_page( [
                                        'page_title' 	=> 'Site General Settings',
                                        'menu_title'	=> 'Site',
                                        'menu_slug' 	=> 'site-general-settings',
                                        'capability'	=> 'edit_posts',
                                        'redirect'	=> false,
                                        'position'      => 4
                                ] );
                        }
                },  5 );


                // $test->register_routes( );
                /*
                if( !isset( $mml ) ) {
                        $mml = new MML( );
                        $mml->initialize( );
                }

                return $mml;
                */
        }

        // initialize
        main( );
//endif;
