<?php

namespace Molamil\Controllers {
        use \WP_REST_Controller;

        class MML_REST_Site_Controller extends WP_REST_Controller
        {
                protected $version;

                public function __construct( )
                {
                        $this->version   = '1';
                        $this->namespace = 'mml/' . 'v' . $this->version;
                        $this->rest_base = __('site', 'mml-wp-api' );

                        // dunno....
                        $this->repo = new \Molamil\Repositories\SiteRepository( );
                }

                public function register_routes( )
                {
                        register_rest_route(
                                $this->namespace, $this->rest_base,
                                [
                                    [
                                        'methods'             => \WP_REST_Server::READABLE,
                                        'callback'            => [ $this, 'get_site' ],
                                        'permission_callback' => [ $this, 'get_item_permissions_check' ],
                                    ]
                                ]
                        );
                }

                public function get_site( $request )
                {
                        // could put menu generation and site generation into their own classes...
                        $menus = $this->get_wp_menus_keyed_by_slug( );

                        $navigation = array_map( function( $menu )
                        {
                                $items = wp_get_nav_menu_items($menu->slug);

                                return array_map( function( $item )
                                {
                                        $object = get_post( $item->object_id );
                                        $type_object = get_post_type_object( $object->post_type );

                                        $label    = $object->post_title;
                                        $slug     = $object->post_name;
                                        $type     = $type_object->name;
                                        $base     = $type_object->rest_base;
                                        $path     = wp_make_link_relative( get_permalink( $object->ID ) );
                                        $template = get_page_template_slug( $object->ID );

                                        return [
                                                'type'     => $type,
                                                'label'    => $label,
                                                'slug'     => $slug,
                                                'path'     => $base . rtrim( $path, '/' ),
                                                'template' => $template
                                        ];
                                }, $items );
                        }, $menus );

                        // dont know how to manage versions...
                        $data = [
                                'navigation' => $navigation,
                                'meta' => [
                                        'location' => parse_url( get_home_url( ) ),
                                        'root'     => '/wp-json/mml/v1/'
                                ]
                        ];

                        $response = rest_ensure_response( $data );
                        return new \WP_REST_Response( $response, 200 );

                }

                public function get_item_permissions_check( $request )
                {
                        return true;
                }

                protected function get_wp_menus_keyed_by_slug( )
                {
                        $menus = array_reduce( wp_get_nav_menus( ), function( $acc, $menu ) {
                                $acc[$menu->slug] = $menu; return $acc;
                        } , []);

                        return $menus;
                }
        }

}
