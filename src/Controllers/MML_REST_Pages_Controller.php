<?php

namespace Molamil\Controllers {
        use \WP_REST_Posts_Controller;
        use \Molamil\Interfaces\IPagesRepository;

        class MML_REST_Pages_Controller extends WP_REST_Posts_Controller 
        {
                protected $version;

                // directly injecting PagesRepository won't make much sense
                // without some way of binding services...anyways.
                public function __construct( IPagesRepository $repository  )
                {
                        $this->post_type = 'page';
                        $this->version   = '1';
                        $this->namespace = 'mml/' . 'v' . $this->version;
                        $this->rest_base = __( 'pages', 'mml-wp-api' );

                        $this->meta = new \WP_REST_Post_Meta_Fields( $this->post_type );

                        $this->repository = $repository;
                }

                public function register_routes( )
                {
                        register_rest_route(
                                $this->namespace, $this->rest_base . '/(?P<slug>[a-z0-9]+(?:-[a-z0-9]+)*)',
                                [
                                        'args' => [
                                                'slug' => [
                                                        'description' => __( 'Unique slug identifier for the page.' ),
                                                        'type'        => 'string',
                                                ],
                                        ],
                                        [
                                                'methods'             => \WP_REST_Server::READABLE,
                                                'callback'            => [ $this, 'get_item_by_slug' ],
                                                'permission_callback' => [ $this, 'get_item_permissions_check' ],
                                                'args'                => [
                                                        'context' => [
                                                            'default' => 'view',
                                                        ],
                                                    ],
                                        ]
                                ]
                        );
                }

                public function get_item_by_slug( $request ) 
                {
                        // all this is a bit convoluted...refactor please...
                        $slug = $request->get_param( 'slug' );
                        $page = $this->repository->get_by_slug( $slug );

                        return $this->render( $page, $request );
                }

                public function get_item_permissions_check( $request )
                {
                        return true;
                }

                protected function render( $page, $request )
                {
                        $data = $this->prepare_item_for_response( $page, $request );

                        $response = rest_get_server( )->response_to_data( $data, false );
                        // run view engine filter....??
                        return new \WP_REST_Response( rest_ensure_response( $response ), 200 );
                }
        }

}
