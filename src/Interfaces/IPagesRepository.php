<?php

namespace Molamil\Interfaces {
        /*
        |--------------------------------------------------------------------------
        | Interface PagesRepository.
        |--------------------------------------------------------------------------
        |
        | TODO: Doc
        |
        */
        interface IPagesRepository {
                public function get_by_slug( string $slug );
        }
}
